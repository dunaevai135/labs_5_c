//
// Created by dunaev on 06.10.17.
//
#include "menu.h"
#include "db.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long level;

char namebuff[BUFFER_SIZE];

struct _menu_t {
	long level;
	long down_level;
	char name[32];
	errors_t (*callback)();
};


errors_t get_car(struct car_t *car) {
	if(get_last_car() == NULL)
		return SET_FILE;
	errors_t err;
	long color;
	long power;
	long month;
	char number[16];
	char name[64];
	printf("Color in decimal: #");
	err = get_long(&color);
	if (err != CONTINUE) {
		return err;
	}
	printf("Power in decimal: ");
	err = get_long(&power);
	if (err != CONTINUE) {
		return err;
	}
	printf("Month in decimal: ");
	err = get_long(&month);
	if (err != CONTINUE) {
		return err;
	}
	err = get_str(number, 16,"Write number: ");
	if (err != CONTINUE) {
		return err;
	}
	err = get_str(name, 64,"Write name: ");
	if (err != CONTINUE) {
		return err;
	}
	car->color = (uint32_t)color;
	car->power = (uint32_t)power;
	car->month = (uint32_t)month;
	strcpy(car->number, number);
	strcpy(car->name, name);
	return CONTINUE;
}

errors_t get_name(char *name) {
	errors_t err = CONTINUE;
	err = get_str(name, BUFFER_SIZE,"Write file name: ");
	if (err != CONTINUE) {
		return err;
	}
	return CONTINUE;
}
errors_t File () {
	printf("TODO File ");
	return CONTINUE;
}

errors_t Newdb () {
	errors_t err = CONTINUE;
	err = get_name(namebuff);
	if (err != CONTINUE) {
		return err;
	}
	err = new_file(namebuff);
	return err;
}

errors_t Load () {
	errors_t err = CONTINUE;
	err = get_name(namebuff);
	if (err != CONTINUE) {
		return err;
	}
	err = load_file(namebuff);
	return err;
}

errors_t Save () {
	if(get_last_car() == NULL) {
		return SET_FILE;
	}
	errors_t err = save_file(EMPTY_FILE);
	return err;
}

errors_t SaveNew () {
	errors_t err = CONTINUE;
	err = get_name(namebuff);
	if (err != CONTINUE) {
		return err;
	}
	err = save_file(namebuff);
	return err;
}

errors_t Change () {
	return CONTINUE;
}

errors_t Add () {
	errors_t err = CONTINUE;
	struct car_t car;
	err = get_car(&car);
	if (err != CONTINUE) {
		return err;
	}
	err = add_car(car);
	return err;
}

errors_t Delete () {
	if(get_last_car() == NULL) {
		return SET_FILE;
	}
	printf("Find before, first will deleted, Write \"0\" if field not needs\n");//TODO grammar
	errors_t err = CONTINUE;
	struct car_t car;
	err = get_car(&car);
	if (err != CONTINUE) {
		return err;
	}
	err = delete_car(car);
	return err;
}

errors_t Find () {
	if(get_last_car() == NULL) {
		return SET_FILE;
	}
	printf("Write <0> if field not needs\n");
	errors_t err = CONTINUE;
	struct car_t car;
	err = get_car(&car);
	if (err != CONTINUE) {
		return err;
	}
	find_car(car);
	return CONTINUE;
}

errors_t Done () {
	if (level == 0)
		return GO_OUT;
	return CONTINUE;
}

const menu_t menu[] = {
	{.level = 0, .down_level = 1, .name = "File", .callback = File},
		{.level = 1, .down_level = 0, .name = "New db", .callback = Newdb},
		{.level = 1, .down_level = 0, .name = "Load", .callback = Load},
		{.level = 1, .down_level = 3, .name = "Save", .callback = NULL},
			{.level = 3, .down_level = 0, .name = "Save same name", .callback = Save},
			{.level = 3, .down_level = 0, .name = "Save in New file", .callback = SaveNew},
	{.level = 0, .down_level = 2, .name = "Change", .callback = Change},
		{.level = 2, .down_level = 0, .name = "Add", .callback = Add},
		{.level = 2, .down_level = 0, .name = "Delete", .callback = Delete},
	{.level = 0, .down_level = 0, .name = "Find", .callback = Find},
	{.level = -1, .down_level = 0, .name = "Done", .callback = Done},
};

const menu_t *work_with_menu(long level, long find) {
	long index = 0;
	for (long i = 0; i < sizeof(menu) / sizeof(menu_t); ++i) {
		if (menu[i].level == level || menu[i].level == -1) {
			index++;
			if (find) {
				if (find == index) {
					return &menu[i];
				}
			} else
				printf("%ld)\t%s\n", index, menu[i].name);
		}
	}
	return NULL;
}

void set_menu_level(long l) {
	level = l;
}

errors_t call_menu_option() {
	long index = 0;
	work_with_menu(level, 0);
	errors_t err = CONTINUE;
	const menu_t *current_menu = NULL;
	do {
		printf("Choose menu option: ");
		err = get_long(&index);
		if (err == CONTINUE) {
			current_menu = work_with_menu(level, index);
		}
		if (err == CONTINUE && current_menu == NULL) {
			err = INVALID;
		}
		say_error(err);
	} while (err != CONTINUE);

	errors_t ret = CONTINUE;
	if (current_menu->callback != NULL)
		ret = current_menu->callback();
	level = current_menu->down_level;

	say_error(ret);
	if(ret == UNEXPECTED || ret == GO_OUT) {
		return ret;
	}
	return CONTINUE;
}
