//
// Created by dunaev on 06.10.17.
//

#ifndef UNTITLED_ERR_H
#define UNTITLED_ERR_H

typedef enum {
	CORRECT = 0,
	INVALID,
	TOBIG,
	NOT_A_NUM,
	TOO_MANY_ARGUMENT,
	UNEXPECTED,
	GO_OUT,
	EXIST,
	CONTINUE,
	SET_FILE
} errors_t;

void say_error(errors_t);

#endif //UNTITLED_ERR_H
