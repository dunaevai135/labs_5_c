//
// Created by dunaev on 06.10.17.
//

#include "err.h"
#include <stdio.h>

const char *errors_arr[] = {
		"",
		"Incorrect command, please try again!",
		"Expression is too big",
		"Incorrect command, Parameter is not number",
		"Incorrect command, to many parameters",
		"Unexpected Error",
		"Try go out...",
		"Leaf does not exist",
		"",
		"Bad idea, provide file before",
};

void say_error(errors_t err) {
	printf("%s", errors_arr[err]);
}
