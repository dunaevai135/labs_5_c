//
// Created by dunaev on 12.10.17.
//

#ifndef LABA_DB_H
#define LABA_DB_H
#include "err.h"
#include <stdlib.h>
#include <stdint.h>

#define EMPTY_FILE "\t"

struct car_t {
	uint32_t id;
	long pos;
	uint32_t color;
	uint32_t power;
	uint32_t month;
	char number[16];
	char name[64];
	struct car_t *second;
};

errors_t new_file(char* name);

errors_t load_file(char *name);

errors_t save_file(char *name);

void free_mem(struct car_t *car);

errors_t add_car(struct car_t);

errors_t find_car(struct car_t);

errors_t delete_car(struct car_t);

errors_t foreach_car(struct car_t car, int(*callback)(struct car_t *) );

struct car_t *get_last_car();

#endif //LABA_DB_H
