//
// Created by dunaev on 06.10.17.
//

#ifndef UNTITLED_IO_H
#define UNTITLED_IO_H
#include "err.h"
#include "db.h"

#define BUFFER_SIZE 256
char buffer[BUFFER_SIZE];

char *skip_space(char *);

errors_t get_str(char *name, int size, char *ask);

errors_t read_in_buffer(char *buffer, int size);

errors_t get_long(long *);

int print_car(struct car_t *);

#endif //UNTITLED_IO_H
