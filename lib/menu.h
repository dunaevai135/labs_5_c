//
// Created by dunaev on 06.10.17.
//

#ifndef UNTITLED_MENU_H
#define UNTITLED_MENU_H
#include <stdint.h>
#include "io.h"
#include "err.h"

typedef struct _menu_t menu_t;

const menu_t *work_with_menu(long, long);

errors_t call_menu_option();

void set_menu_level(long);
#endif //UNTITLED_MENU_H
