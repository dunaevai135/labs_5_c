//
// Created by dunaev on 06.10.17.
//

#include "io.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


void flush_input(void) {
	char c;
	while (scanf("%c", &c) == 1 && c != '\n');
}

errors_t get_str(char *name, int size, char *ask) {
	errors_t err = CONTINUE;
	puts(ask);
	err = read_in_buffer(name, BUFFER_SIZE);
	if (err != CONTINUE) {
		return err;
	}
	name[strlen(name)-1] = 0;
	return err;
}

errors_t read_in_buffer(char *buffer, int size) {
	if (fgets(buffer, size, stdin) == NULL) {
		return INVALID;
	}
	if (strlen(buffer) >= size - 2) {
		ungetc('1', stdin);
		flush_input();
		return TOBIG;
	}
	return CONTINUE;
}

errors_t get_answer_from_buffer(char *buf, long *X) {
	buf = skip_space(buf);
	char *end;
	*X = strtol(buf, &end, 10);
	if (buf == end)
		return NOT_A_NUM;
	if (skip_space(end) == end && *end != '\n')
		return NOT_A_NUM;
	return CONTINUE;
}

errors_t get_long(long *X) {
	errors_t err;
	err = read_in_buffer(buffer, BUFFER_SIZE);
	if (err != CONTINUE)
		return err;
	err = get_answer_from_buffer(buffer, X);
	if (err != CONTINUE)
		return err;
}

char *skip_space(char *start) {
	int i = 0;
	while ((start[i] == ' ' || start[i] == '\t' || start[i] == '\v' || start[i] == '\f') && start[i] != '\0') {
		i++;
	}
	return &start[i];
}

int print_car(struct car_t *car) {
	printf("struct car_t {\n"
			       "\tuint32_t id = %d;\n"
			       "\tuint32_t color = %d;\n"
			       "\tuint32_t power = %d;\n"
			       "\tuint32_t month = %d;\n"
			       "\tchar number[16] = %s;\n"
			       "\tchar name[64] = %s;\n"
			       "};", car->id, car->color, car->power, car->month, car->number, car->name);
	return 0;
}