//
// Created by dunaev on 12.10.17.
//

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include "db.h"
#include "io.h"

struct car_t *first_car = NULL, *last_car = NULL;

struct car_t null_car = {0,0,0,0,0,0,0};

char old_name[BUFFER_SIZE];

FILE *file = NULL;

uint64_t car_count = 0;

uint32_t id = 0;

errors_t read_all_cars(struct car_t *car) {
	for (uint64_t i = 0; i < car_count; ++i) {
		fread(car, sizeof(struct car_t)-sizeof(struct car_t*), 1, file);
		car->second = (struct car_t *)malloc(sizeof(struct car_t));
		car = car->second;
	}
	car->second = NULL;
	last_car = car;
	return CONTINUE;
}

errors_t write_all_cars(struct car_t *car) {
	for (uint64_t i = 0; i < car_count; ++i) {
		fwrite(car, sizeof(struct car_t)-sizeof(struct car_t*), 1, file);//TODO error check
		car = car->second;
	}
	return CONTINUE;
}

errors_t new_file(char* name) {
	file = fopen(name, "wb");
	strcpy(old_name, name);
	fwrite(&null_car, sizeof(car_count), 1, file);
	fclose(file);
	first_car = (struct car_t *)malloc(sizeof(struct car_t));
	*first_car = null_car;
	last_car = first_car;
	return CONTINUE;
}

errors_t load_file(char* name) {
	free_mem(first_car);
	file = fopen(name, "rb");
	strcpy(old_name, name);
	if (file == NULL) {
		printf("File “%s” not found.\n", name);
		return CONTINUE;
	}
	first_car = (struct car_t *)malloc(sizeof(struct car_t));
	*first_car = null_car;
	last_car = first_car;

	fread(&car_count, sizeof(car_count), 1, file);

	read_all_cars(last_car); //TODO just N

	fclose(file);
	return CONTINUE;
}

errors_t save_file(char* name) {
	if (strcmp(name, EMPTY_FILE) == 0) {
		file = fopen(old_name, "wb");
	}
	else {
		strcpy(old_name, name);
		file = fopen(name, "wb");
	}
	if (file == NULL) {
		printf("File “%s” can`t be open.\n", name);
		return INVALID;
	}
	fwrite(&car_count, sizeof(car_count), 1, file);
	write_all_cars(first_car);
	free_mem(first_car);
	first_car = NULL;
	last_car = NULL;
	fclose(file);

	return CONTINUE;
}

void free_mem(struct car_t *car) {
	struct car_t *ptmp;
	for (uint64_t i = 0; i < car_count; ++i) {
		ptmp = car->second;
		free(car);
		car = ptmp;
	}
}

errors_t add_car(struct car_t car) {
	if(last_car == NULL)
		return SET_FILE;
	car_count++;
	*(last_car) = car;
	last_car->id = id++;
	last_car->second = (struct car_t *)malloc(sizeof(struct car_t));//TODO err check
	last_car = last_car->second;
	last_car->second = NULL;
	return CONTINUE;
}

int cmpr_car(const struct car_t *a, const struct car_t *b) {
	int must = 5;
	int real = 0;
	if(strcmp(a->name, "0") == 0 || strcmp(b->name, "0") == 0) {
		must--;
	}
	if(strcmp(a->number, "0") == 0 || strcmp(b->number, "0") == 0) {
		must--;
	}
	if(a->color == 0 || b->color == 0) {
		must--;
	}
	if(a->month == 0 || b->month == 0) {
		must--;
	}
	if(a->power == 0 || b->power == 0) {
		must--;
	}

	if(strcmp(a->name, b->name) == 0 ) {
		real++;
	}
	if(strcmp(a->number, b->number) == 0 ) {
		real++;
	}
	if(a->color == b->color ) {
		real++;
	}
	if(a->month == b->month ) {
		real++;
	}
	if(a->power == b->power ) {
		real++;
	}

	return must - real;
}

int del_car(struct car_t *car) {
	struct car_t *ptmp;
	ptmp = first_car;
	for (uint64_t i = 0; i < car_count; ++i) {
		if (ptmp->second == car) {
			break;
		}
		ptmp = ptmp->second;
	}
	ptmp->second = car->second;
	free(car);
	return 1;
}

errors_t find_car(struct car_t car) {
	return foreach_car(car, print_car);
}

errors_t delete_car(struct car_t car) {
	return foreach_car(car, del_car);
}

errors_t foreach_car(struct car_t car, int(*callback)(struct car_t *) ) {
	if(last_car == NULL)
		return SET_FILE;
	struct car_t *ptmp;
	ptmp = first_car;
	for (uint64_t i = 0; i < car_count; ++i) {
		if (cmpr_car(&car, ptmp) == 0) {
			if (callback(ptmp))
				break;
		}
		ptmp = ptmp->second;
	}
	return CONTINUE;
}

struct car_t * get_last_car() {
	return last_car;
}
