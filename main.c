//
// Created by dunaev on 06.10.17.
//

#include <stdio.h>
#include <stdint.h>
#include "menu.h"

int main(int argc, char const *argv[]) {
	errors_t r= CONTINUE;
	set_menu_level(0);
	while (r == CONTINUE) {
		r = call_menu_option();
		printf("\n");
	}
	return 0;
}